/**
 * SPDX-FileCopyrightText: (c) 2020 Carl Schwan <carl@carlschwan.eu>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import ECMAScript modules
const $ = require('jquery');
global.$ = global.jQuery = require('jquery');
require('bootstrap');
require('ekko-lightbox');

// Import SASS modules
require('./../css/bootstrap.scss');

document.querySelectorAll('.preview').forEach(preview => {
    preview.addEventListener('click', () => {
        const imagepreview = document.querySelector('#kImagePreview img');
        imagepreview.setAttribute('src', preview.getAttribute('src'));
        imagepreview.setAttribute('alt', preview.getAttribute('alt'));
        $("#kImagePreview").modal('show');
    });
});

// add autoplay for tabs video
document.querySelectorAll('.tab-pane.video').forEach(function(tab) {
    tab = tab.id;
    const nav = document.getElementById(`${tab}-control`);
    const video = document.querySelector(`#${tab} video`);
    const button = document.querySelector(`#${tab} button`);

    nav.addEventListener('click', function() {
        if (!button.classList.contains('d-none')) {
            button.classList.add('d-none');
        }
        video.currentTime = 0;
        video.play();
    });

    video.addEventListener('ended', function() {
        if (button.classList.contains('d-none')) {
            button.classList.remove('d-none');
        }
    });

    button.addEventListener('click', function() {
        button.classList.add('d-none');
        video.currentTime = 0;
        video.play();
    });
});

// add autoplay for tabs with two videos (laptop and mobile)
document.querySelectorAll('.tab-pane.two-videos').forEach(function(tab) {
    tab = tab.id;
    const nav = document.getElementById(`${tab}-control`);
    const video_mobile = document.querySelector(`#${tab} .phone-overlay video`);
    const video_laptop = document.querySelector(`#${tab} .laptop-overlay video`);
    const button = document.querySelector(`#${tab} button`);

    nav.addEventListener('click', function() {
        if (!button.classList.contains('d-none')) {
            button.classList.add('d-none');
        }
        video_mobile.currentTime = 0;
        video_laptop.currentTime = 0;
        video_mobile.play();
        video_laptop.play();
    });

    video_mobile.addEventListener('ended', function() {
        if (button.classList.contains('d-none')) {
            button.classList.remove('d-none');
        }
    });

    button.addEventListener('click', function() {
        button.classList.add('d-none');
        video_mobile.currentTime = 0;
        video_laptop.currentTime = 0;
        video_mobile.play();
        video_laptop.play();
    });
});

// create config object: rootMargin and threshold
// are two properties exposed by the interface
const config = {
  rootMargin: '0px 0px 50px 0px',
  threshold: 0
};

// register the config object with an instance
// of intersectionObserver
let observer = new IntersectionObserver(function(entries, self) {
    // iterate over each entry
    entries.forEach(entry => {
        // process just the images that are intersecting.
        // isIntersecting is a property exposed by the interface
        if(entry.isIntersecting) {
            // custom function that copies the path to the img
            // from data-src to src
            entry.target.src = entry.target.dataset.src;
            // the image is now in place, stop watching
            self.unobserve(entry.target);
        }
    });
}, config);


const imgs = document.querySelectorAll('[data-src]');
imgs.forEach(img => {
    observer.observe(img);
});

document.querySelectorAll('.nav-features .nav-link').forEach(nav => {
    nav.addEventListener('mouseover', function(event) {
        const tab_content = document.querySelector(`${event.target.getAttribute("href")} [data-src]`);
        if (tab_content) {
            tab_content.src = tab_content.dataset.src;
        }
    });
});

$(document).on('click', '[data-toggle="lightbox"]', function(event) {
    event.preventDefault();
    $(this).ekkoLightbox();
});


